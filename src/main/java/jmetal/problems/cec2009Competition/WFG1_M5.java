//  CEC2009_WFG1_M5.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.problems.cec2009Competition;


import jmetal.core.*;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;

/**
 * Class representing problem CEC2009_WFG1_M5
 */
@SuppressWarnings("serial")
public class WFG1_M5 extends Problem {

    final double EPSILON = 1.0e-10;

    /**
     * Constructor. Creates a default instance of problem CEC2009_WFG1_M5 (30
     * decision variables)
     */
    public WFG1_M5(String solutionType) throws ClassNotFoundException {
        this(solutionType, 30); // 30 variables by default;
    }

    /**
     * Creates a new instance of problem CEC2009_WFG1_M5.
     *
     * @param numberOfVariables Number of variables.
     */
    public WFG1_M5(String solutionType, Integer numberOfVariables) throws ClassNotFoundException {
        numberOfVariables_ = numberOfVariables.intValue();
        numberOfObjectives_ = 5;
        numberOfConstraints_ = 0;
        problemName_ = "WFG1_M5";

        upperLimit_ = new double[numberOfVariables_];
        lowerLimit_ = new double[numberOfVariables_];

        for (int var = 0; var < numberOfVariables_; var++) {
            lowerLimit_[var] = 0.0;
            upperLimit_[var] = 1.0;
        } //for

        if (solutionType.compareTo("BinaryReal") == 0)
            solutionType_ = new BinaryRealSolutionType(this);
        else if (solutionType.compareTo("Real") == 0)
            solutionType_ = new RealSolutionType(this);
        else {
            System.out.println("Error: solution type " + solutionType + " invalid");
            System.exit(-1);
        }
    }

    /**
     * Evaluate() method
     */
    @Override
    public void evaluate(Solution solution) throws JMException {
        Variable[] decisionVariables = solution.getDecisionVariables();

        double[] x = new double[numberOfVariables_];
        for (int i = 0; i < numberOfVariables_; i++)
            x[i] = decisionVariables[i].getValue();

        int i, j;

        double[] y = new double[30];
        double[] t1 = new double[30];
        double[] t2 = new double[30];
        double[] t3 = new double[30];
        double[] t4 = new double[5];

        final int k = getNumberOfObjectives() == 2 ? 4 : 2 * (getNumberOfObjectives() - 1);

        for (i = 0; i < getNumberOfVariables(); i++) {
            y[i] = x[i] / (2.0 * (i + 1));
        }

        // y = WFG1_t1(y, k);
        for (i = 0; i < k; i++) {
            t1[i] = y[i];
        }

        for (i = k; i < getNumberOfVariables(); i++) {
            t1[i] = s_linear(y[i], 0.35);
        }

        // y = WFG1_t2(y, k);
        for (i = 0; i < k; i++) {
            t2[i] = t1[i];
        }

        for (i = k; i < getNumberOfVariables(); i++) {
            t2[i] = b_flat(t1[i], 0.8, 0.75, 0.85);
        }

        // y = WFG1_t3(y);
        for (i = 0; i < getNumberOfVariables(); i++) {
            t3[i] = b_poly(t2[i], 0.02);
        }

        // y = WFG1_t4( y,k,getNumberOfObjectives(),getNumberOfVariables());
        {
            double[] w = new double[30];
            double[] y_sub = new double[30];
            double[] w_sub = new double[30];
            double[] y_sub2 = new double[30];
            double[] w_sub2 = new double[30];

            for (i = 1; i <= getNumberOfVariables(); i++) {
                w[i - 1] = 2.0 * i;
            }

            for (i = 1; i <= getNumberOfObjectives() - 1; i++) {
                final int head = (i - 1) * k / (getNumberOfObjectives() - 1);
                final int tail = i * k / (getNumberOfObjectives() - 1);

                for (j = head; j < tail; j++) {
                    y_sub[j - head] = t3[j];
                    w_sub[j - head] = w[j];
                }
                t4[i - 1] = r_sum(y_sub, w_sub, tail - head);
            }

            for (j = k; j < getNumberOfVariables(); j++) {
                y_sub2[j - k] = t3[j];
                w_sub2[j - k] = w[j];
            }
            t4[i - 1] = r_sum(y_sub2, w_sub2, getNumberOfVariables() - k);
        }
        // shape
        {
            int m;
            short[] A = new short[5];
            double[] xx = new double[5];
            double[] h = new double[5];
            double[] S = new double[5];

            A[0] = 1;
            for (i = 1; i < getNumberOfObjectives() - 1; i++) {
                A[i] = 1;
            }

            for (i = 0; i < getNumberOfObjectives() - 1; i++) {
                double tmp1;
                tmp1 = t4[getNumberOfObjectives() - 1];
                if (A[i] > tmp1) {
                    tmp1 = A[i];
                }
                xx[i] = tmp1 * (t4[i] - 0.5) + 0.5;
            }
            xx[getNumberOfObjectives() - 1] = t4[getNumberOfObjectives() - 1];

            for (m = 1; m <= getNumberOfObjectives() - 1; m++) {
                h[m - 1] = convex(xx, m, getNumberOfObjectives());
            }
            h[m - 1] = mixed(xx, 5, 1.0);

            for (m = 1; m <= getNumberOfObjectives(); m++) {
                S[m - 1] = m * 2.0;
            }

            for (i = 0; i < getNumberOfObjectives(); i++) {
                solution.setObjective(i, 1.0 * xx[getNumberOfObjectives() - 1] + S[i] * h[i]);
            }
        }
    }

    /////////////////////////////////////////////////////////////////////
    // s_linear function
    double s_linear(double y, double A) {
        return correct_to_01(Math.abs(y - A) / Math.abs(Math.floor(A - y) + A), EPSILON);
    }

    /////////////////////////////////////////////////////////////////////
    // correct_to_01 function
    double correct_to_01(double aa, double epsilon) {
        double min = 0.0, max = 1.0;
        double min_epsilon = min - epsilon;
        double max_epsilon = max + epsilon;

        if (aa <= min && aa >= min_epsilon) {
            return min;
        } else if (aa >= max && aa <= max_epsilon) {
            return max;
        } else {
            return aa;
        }
    }

    /////////////////////////////////////////////////////////////////////
    // b_flat function
    double b_flat(double y, double A, double B, double C) {
        double tmp1 = min_double(0.0, Math.floor(y - B)) * A * (B - y) / B;
        double tmp2 = min_double(0.0, Math.floor(C - y)) * (1.0 - A) * (y - C) / (1.0 - C);

        return correct_to_01(A + tmp1 - tmp2, EPSILON);
    }

    /////////////////////////////////////////////////////////////////////
    // min_double function
    double min_double(double aa, double bb) {
        return aa < bb ? aa : bb;
    }

    /////////////////////////////////////////////////////////////////////
    // b_poly function
    double b_poly(double y, double alpha) {
        return correct_to_01(Math.pow(y, alpha), EPSILON);
    }

    /////////////////////////////////////////////////////////////////////
    // r_sum function
    double r_sum(double[] y, double[] w, int ny) {

        int i;
        double numerator = 0.0;
        double denominator = 0.0;

        for (i = 0; i < ny; i++) {
            numerator += w[i] * y[i];
            denominator += w[i];
        }

        return correct_to_01(numerator / denominator, EPSILON);
    }

    /////////////////////////////////////////////////////////////////////
    // convex function
    double convex(double[] x, int m, int M) {
        int i;
        double result = 1.0;
        for (i = 1; i <= M - m; i++) {
            result *= 1.0 - Math.cos(x[i - 1] * Math.PI / 2.0);
        }

        if (m != 1) {
            result *= 1.0 - Math.sin(x[M - m] * Math.PI / 2.0);
        }

        return correct_to_01(result, EPSILON);
    }

    /////////////////////////////////////////////////////////////////////
    // convex function
    double mixed(double[] x, int A, double alpha) {
        double tmp = 2.0 * A * Math.PI;
        return correct_to_01(Math.pow(1.0 - x[0] - Math.cos(tmp * x[0] + Math.PI / 2.0) / tmp, alpha), EPSILON);
    }

}
