/**
 * MOEAD_main.java
 *
 * @author Ke Li <keli.genius@gmail.com>
 * <p>
 * Copyright (c) 2012 Ke Li
 * <p>
 * Note: This is a free software developed based on the open source project
 * jMetal<http://jmetal.sourceforge.net>. The copy right of jMetal belongs to
 * its original authors, Antonio J. Nebro and Juan J. Durillo. Nevertheless,
 * this current version can be redistributed and/or modified under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//  MOEAD_main.java
//
//  Author:

//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.moead;

import jmetal.core.*;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.*;
import jmetal.operators.mutation.*;
import jmetal.problems.DTLZ.*;
import jmetal.problems.LZ09.*;
import jmetal.problems.ZDT.*;
import jmetal.problems.cec2009Competition.*;
import jmetal.util.Configuration;
import jmetal.util.Global;
import jmetal.util.JMException;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import static jmetal.util.Global.prepareOutputDirectory;
import static org.uma.jmetal.algorithm.multiobjective.moead.util.MOEADUtils.getSubsetOfEvenlyDistributedSolutions;


public class MOEAD_main {
    public static Logger logger_; // Logger object
    public static FileHandler fileHandler_; // FileHandler object
    private static final int INDEPENDENT_RUNS = 30;

    /**
     * @param args Command line arguments. The first (optional) argument
     *             specifies the problem to solve.
     * @throws JMException
     * @throws IOException
     * @throws SecurityException      Usage: three options - jmetal.metaheuristics.moead.MOEAD_main
     *                                - jmetal.metaheuristics.moead.MOEAD_main problemName -
     *                                jmetal.metaheuristics.moead.MOEAD_main problemName
     *                                ParetoFrontFile
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws JMException,
            SecurityException, IOException, ClassNotFoundException {


        List<Problem> problemList = new ArrayList<>();
        problemList.add(new ZDT1("Real"));
        problemList.add(new ZDT2("Real"));
        problemList.add(new ZDT3("Real"));
        problemList.add(new ZDT4("Real"));
        problemList.add(new ZDT6("Real"));
        problemList.add(new CEC2009_UF1("Real"));
        problemList.add(new CEC2009_UF2("Real"));
        problemList.add(new CEC2009_UF3("Real"));
        problemList.add(new CEC2009_UF4("Real"));
        problemList.add(new CEC2009_UF5("Real"));
        problemList.add(new CEC2009_UF6("Real"));
        problemList.add(new CEC2009_UF7("Real"));
        problemList.add(new CEC2009_UF8("Real"));
        problemList.add(new CEC2009_UF9("Real"));
        problemList.add(new CEC2009_UF10("Real"));
        problemList.add(new R2_DTLZ2_M5("Real"));
        problemList.add(new R3_DTLZ3_M5("Real"));
        problemList.add(new WFG1_M5("Real"));
        problemList.add(new DTLZ1("Real"));
        problemList.add(new DTLZ2("Real"));
        problemList.add(new DTLZ3("Real"));
        problemList.add(new DTLZ4("Real"));
        problemList.add(new DTLZ5("Real"));
        problemList.add(new DTLZ6("Real"));
        problemList.add(new DTLZ7("Real"));
        problemList.add(new LZ09_F1("Real"));
        problemList.add(new LZ09_F2("Real"));
        problemList.add(new LZ09_F3("Real"));
        problemList.add(new LZ09_F4("Real"));
        problemList.add(new LZ09_F5("Real"));
        problemList.add(new LZ09_F6("Real"));
        problemList.add(new LZ09_F7("Real"));
        problemList.add(new LZ09_F8("Real"));
        problemList.add(new LZ09_F9("Real"));

        Problem problem; // The problem to solve
        Algorithm algorithm; // The algorithm to use
        Operator crossover; // Crossover operator
        Operator mutation; // Mutation operator

        HashMap parameters; // Operator parameters

        // Logger object and file to store log messages
        logger_ = Configuration.logger_;
        fileHandler_ = new FileHandler("MOEAD.log");
        logger_.addHandler(fileHandler_);


        for (Problem p: problemList
        ) {

            problem = p;

            // 创建文件夹
            String outputDirectoryName = "../data/bandits/" + problem.getName();
            prepareOutputDirectory(outputDirectoryName);

//            algorithm = new MOEAD(problem);
//		algorithm = new MOEAD_DRA(problem);
            algorithm = new MOEADDRA_MAB(problem);

            // Algorithm parameters
            if (problem.getNumberOfObjectives() == 2) {
                algorithm.setInputParameter("populationSize", 600);
                algorithm.setInputParameter("maxEvaluations", 30000);
            } else if (problem.getNumberOfObjectives() == 3) {
                algorithm.setInputParameter("populationSize", 1000);
                algorithm.setInputParameter("maxEvaluations", 50000);
            } else if (problem.getNumberOfObjectives() == 5) {
                algorithm.setInputParameter("populationSize", 1500);
                algorithm.setInputParameter("maxEvaluations", 300000);
            }

            algorithm.setInputParameter("dataDirectory", "src/weight");

            // Crossover operator
            parameters = new HashMap();
            parameters.put("CR", 1.0);
            parameters.put("F", 0.5);
            crossover = CrossoverFactory.getCrossoverOperator(
                    "DifferentialEvolutionCrossover", parameters);

            // Mutation operator
            parameters = new HashMap();
            parameters.put("probability", 1.0 / problem.getNumberOfVariables());
            parameters.put("distributionIndex", 20.0);
            mutation = MutationFactory.getMutationOperator("PolynomialMutation",
                    parameters);

            algorithm.addOperator("crossover", crossover);
            algorithm.addOperator("mutation", mutation);

            // Add the indicator object to the algorithm
            for (int i = 0; i < INDEPENDENT_RUNS; i++) {
                Global.RUN = i;

                String funFile = outputDirectoryName + "/FUN" + i + ".tsv";
                String varFile = outputDirectoryName + "/VAR" + i + ".tsv";

                // Execute the Algorithm
                long initTime = System.currentTimeMillis();
                System.out.println(problem.getName() + " : The " + i + " run");
//                SolutionSet population = algorithm.execute();
                SolutionSet population = convert(algorithm.execute(), p, 100);


                long estimatedTime = System.currentTimeMillis() - initTime;

                // Result messages
                logger_.info("Total execution time: " + estimatedTime + "ms");
                logger_.info("Variables values have been writen to file VAR");
                population.printVariablesToFile(varFile);
                logger_.info("Objectives values have been writen to file FUN");
                population.printObjectivesToFile(funFile);
            }
        }
    } // main

    public static SolutionSet convert(SolutionSet population_, Problem problem_, int size) throws JMException, ClassNotFoundException {
        // 将 population_ 转化为 jmetal 下的 population， 并输出
        List<DoubleSolution> jmPopulation = new ArrayList(population_.size());
        int m = problem_.getNumberOfObjectives();
        int n = problem_.getNumberOfVariables();

        DoubleProblem problem = new org.uma.jmetal.problem.multiobjective.dtlz.DTLZ1(n, m);

        for (int i = 0; i < population_.size(); i++) {
            DoubleSolution solution = problem.createSolution();

            for (int j = 0; j < problem_.getNumberOfObjectives(); j++) {
                solution.setObjective(j, population_.get(i).getObjective(j));
            }

            for (int j = 0; j < problem_.getNumberOfVariables(); j++) {
                solution.setVariableValue(j ,population_.get(i).getDecisionVariables()[j].getValue());
            }

            jmPopulation.add(solution);
        }

        List<DoubleSolution> retPopulation = getSubsetOfEvenlyDistributedSolutions(jmPopulation, size);
        SolutionSet retPopulation_ = new SolutionSet(retPopulation.size());
        for (int i = 0; i < retPopulation.size(); i++) {
            Solution solution = new Solution(problem_);

            for (int j = 0; j < problem_.getNumberOfObjectives(); j++) {
                solution.setObjective(j, retPopulation.get(i).getObjective(j));
            }

            for (int j = 0; j < problem_.getNumberOfVariables(); j++) {
                solution.getDecisionVariables()[j].setValue(retPopulation.get(i).getVariableValue(j));
            }

            retPopulation_.add(solution);
        }

        return retPopulation_;
    }

} // MOEAD_main
