package jmetal.metaheuristics.moead;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.problems.DTLZ.*;
import jmetal.problems.LZ09.*;
import jmetal.problems.ZDT.*;
import jmetal.problems.cec2009Competition.*;
import jmetal.util.JMException;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static jmetal.util.Configuration.logger_;
import static org.uma.jmetal.algorithm.multiobjective.moead.util.MOEADUtils.getSubsetOfEvenlyDistributedSolutions;

/**
 * @Author: Zhi-Ming Dong, dzm.neu@gmail.com
 * @Date: created in 18-7-13 15:05
 * @Version: v
 * @Descriptiom: #
 * 1#
 * @Modified by:
 */
public class MOEADFRRMABBuilder {
    private String problemName;
    private int maxEvaluations;
    private int populationSize;
    private int resultPopulationSize;

    public MOEADFRRMABBuilder(String problemName, int maxEvaluations, int populationSize, int resultPopulationSize) {
        this.problemName = problemName;
        this.maxEvaluations = maxEvaluations;
        this.populationSize = populationSize;
        this.resultPopulationSize = resultPopulationSize;

    }

    public List<DoubleSolution> execute() throws JMException, ClassNotFoundException {
        Problem problem; // The problem to solve
        Algorithm algorithm; // The algorithm to use
        Operator crossover; // Crossover operator
        Operator mutation; // Mutation operator

        HashMap parameters; // Operator parameters

        problem = ensureProblem(problemName);

//            algorithm = new MOEAD(problem);
//		algorithm = new MOEAD_DRA(problem);
        algorithm = new MOEADDRA_MAB(problem);

        // Algorithm parameters
        algorithm.setInputParameter("populationSize", populationSize);
        algorithm.setInputParameter("maxEvaluations", maxEvaluations);

        algorithm.setInputParameter("dataDirectory", "src/weight");

        // Crossover operator
        parameters = new HashMap();
        parameters.put("CR", 1.0);
        parameters.put("F", 0.5);
        crossover = CrossoverFactory.getCrossoverOperator(
                "DifferentialEvolutionCrossover", parameters);

        // Mutation operator
        parameters = new HashMap();
        parameters.put("probability", 1.0 / problem.getNumberOfVariables());
        parameters.put("distributionIndex", 20.0);
        mutation = MutationFactory.getMutationOperator("PolynomialMutation",
                parameters);

        algorithm.addOperator("crossover", crossover);
        algorithm.addOperator("mutation", mutation);

//                SolutionSet population = algorithm.execute();
//            SolutionSet population = convert(algorithm.execute(), p, 100);

        return convert2(algorithm.execute(), problem, resultPopulationSize);
    }

    private List<DoubleSolution> convert2(SolutionSet population_, Problem problem_, int resultPopulationSize) throws JMException {
        List<DoubleSolution> jmPopulation = new ArrayList(population_.size());
        int m = problem_.getNumberOfObjectives();
        int n = problem_.getNumberOfVariables();

        DoubleProblem problem = new org.uma.jmetal.problem.multiobjective.dtlz.DTLZ1(n, m);

        for (int i = 0; i < population_.size(); i++) {
            DoubleSolution solution = problem.createSolution();

            for (int j = 0; j < problem_.getNumberOfObjectives(); j++) {
                solution.setObjective(j, population_.get(i).getObjective(j));
            }

            for (int j = 0; j < problem_.getNumberOfVariables(); j++) {
                solution.setVariableValue(j ,population_.get(i).getDecisionVariables()[j].getValue());
            }

            jmPopulation.add(solution);
        }

        return getSubsetOfEvenlyDistributedSolutions(jmPopulation, resultPopulationSize);
    }

    private Problem ensureProblem(String problemName) throws ClassNotFoundException {

        switch (problemName) {
            case "UF1":
                return new CEC2009_UF1("Real");
            case "UF2":
                return new CEC2009_UF2("Real");
            case "UF3":
                return new CEC2009_UF3("Real");
            case "UF4":
                return new CEC2009_UF4("Real");
            case "UF5":
                return new CEC2009_UF5("Real");
            case "UF6":
                return new CEC2009_UF6("Real");
            case "UF7":
                return new CEC2009_UF7("Real");
            case "UF8":
                return new CEC2009_UF8("Real");
            case "UF9":
                return new CEC2009_UF9("Real");
            case "UF10":
                return new CEC2009_UF10("Real");
            case "ZDT1":
                return new ZDT1("Real");
            case "ZDT2":
                return new ZDT2("Real");
            case "ZDT3":
                return new ZDT3("Real");
            case "ZDT4":
                return new ZDT4("Real");
            case "ZDT6":
                return new ZDT6("Real");
            case "DTLZ1":
                return new DTLZ1("Real");
            case "DTLZ2":
                return new DTLZ2("Real");
            case "DTLZ3":
                return new DTLZ3("Real");
            case "DTLZ4":
                return new DTLZ4("Real");
            case "DTLZ5":
                return new DTLZ5("Real");
            case "DTLZ6":
                return new DTLZ6("Real");
            case "DTLZ7":
                return new DTLZ7("Real");
            case "LZ09F1":
                return new LZ09_F1("Real");
            case "LZ09F2":
                return new LZ09_F2("Real");
            case "LZ09F3":
                return new LZ09_F3("Real");
            case "LZ09F4":
                return new LZ09_F4("Real");
            case "LZ09F5":
                return new LZ09_F5("Real");
            case "LZ09F6":
                return new LZ09_F6("Real");
            case "LZ09F7":
                return new LZ09_F7("Real");
            case "LZ09F8":
                return new LZ09_F8("Real");
            case "LZ09F9":
                return new LZ09_F9("Real");
            default:
                logger_.info("No " + problemName + " ...");
                return null;
        }


    }
}
