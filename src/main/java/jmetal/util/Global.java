package jmetal.util;

import java.io.File;

import static jmetal.util.Configuration.logger_;

/**
 * @Author: ZhimingDong dzm_neu@163.com
 * @Descriptiom:
 * @Date: created in 2017/12/3 13:08
 * @Modified by:
 */
public class Global {
    public static int RUN;

    public static void prepareOutputDirectory(String outputDirectoryName) {
        if (experimentDirectoryDoesNotExist(outputDirectoryName)) {
            createExperimentDirectory(outputDirectoryName);
        }
    }

    static boolean experimentDirectoryDoesNotExist(String outputDirectoryName) {
        boolean result;
        File experimentDirectory;

        experimentDirectory = new File(outputDirectoryName);
        if (experimentDirectory.exists() && experimentDirectory.isDirectory()) {
            result = false;
        } else {
            result = true;
        }

        return result;
    }

    static void createExperimentDirectory(String outputDirectoryName) {
        File experimentDirectory;
        experimentDirectory = new File(outputDirectoryName);

        if (experimentDirectory.exists()) {
            experimentDirectory.delete();
        }

        boolean result;
        result = new File(outputDirectoryName).mkdirs();
        if (!result) {
            logger_.info("Error creating experiment directory: " + outputDirectoryName);
        }
    }
}